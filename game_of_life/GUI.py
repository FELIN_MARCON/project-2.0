import tkinter as tk
import MVP
from tkinter import messagebox

def default():
    int_variables = []  # array that will store the int values
    str_variables = []  # array that will store the str values

    if len(seed_pos_x.get()) != 0:  # checks if the entry is not null
        try:
            seed_pos_x_int = int(seed_pos_x.get())
        except:
            print(
                "This value must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it (: )")  # message error
            messagebox.showerror("INVALID VALUE",
                                 "Position's value value must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it ) \n\n\n\n (ᵔᴥᵔ) ")

    else:
        seed_pos_x_int = 0  # if it is null, atributes a default value
    int_variables.append(seed_pos_x_int)

    if len(seed_pos_y.get()) != 0:  # checks if the entry is not null
        try:
            seed_pos_y_int = int(seed_pos_y.get())
        except:
            print(
                "This value must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it (: )")
            messagebox.showerror("INVALID VALUE",
                                 "Position's value must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it \n\n\n\n ¯\_(ツ)_/¯ )")
    else:
        seed_pos_y_int = 0
    int_variables.append(seed_pos_y_int)

    if len(generations.get()) != 0:
        try:
            generations_int = int(generations.get())
        except:
            print(
                "The number of generations' value must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it)   ಠ‿↼")
            messagebox.showerror("INVALID VALUE",
                                 "The number of generations' value must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it) \n\n\n\n  ಠ‿↼")
    else:
        generations_int = 30
    int_variables.append(generations_int)

    if len(colormap.get()) != 0:
        colormap_str = str(colormap.get())
    else:
        colormap_str = "gray"
    str_variables.append(colormap_str)

    if len(interval.get()) != 0:
        try:
            interval_int = int(interval.get())
        except:
            print(
                "The interval's value (in miliseconds) must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it) \n\n\n\n (: ")
            messagebox.showerror(
                "The interval's value (in miliseconds) must be an integer, please, re-run the program and give a valide value (or no value, you can just leave it to the system to define it (: )")
    else:
        interval_int = 300
    int_variables.append(interval_int)
    return int_variables, str_variables

def run():
    int_values, str_values = default()
    MVP.game_generator(int(univ_size.get()), seed_type.get(),
                       (int_values[1], int_values[0]),
                       int_values[2], str_values[0],
                       save_var.get(), int_values[3])

#============ MAIN WINDOW AND FRAME ========================
window= tk.Tk()
window.geometry('300x600')
window.title("Game of Life")
window.minsize(300,600)

main_frame = tk.Frame(window, bd=15, relief='sunken')
main_frame.pack(expand=1, fill='both')

#============ FRAMES FOR WIDGETS ====================
middle = tk.Frame(main_frame, )
middle.pack(fill='both', anchor='n')

header_frame = tk.Frame(middle)
header_frame.pack(fill='both',expand=1,anchor='n')

param_frame = tk.Frame(middle)
param_frame.pack(side='top')

par_name_frame = tk.Frame(param_frame)
par_name_frame.pack(fill='both', side='left',anchor='w')

param_entries_frame = tk.Frame(param_frame)
param_entries_frame.pack(fill='both', side='right', anchor='e')

button_frame = tk.Frame(middle)
button_frame.pack(fill='both',anchor='s', side='bottom')

#-------------- header -----------------------------------
header = tk.Label(header_frame, text='Game of Life', pady=15, font=('System', 26, 'bold'), relief='sunken', bd=15)
header.pack(fill='both', expand=1)

univ_size = tk.Entry(param_entries_frame)
univ_size.pack(fill='both',padx=10, pady=10)
univ_size_label = tk.Label(par_name_frame,text='Universe size *',padx=10, pady=10)
univ_size_label.pack(fill='both')

seed_type = tk.Entry(param_entries_frame)
seed_type.pack(fill='y',padx=10, pady=10)
seed_label = tk.Label(par_name_frame,text='Seed *',pady=10)
seed_label.pack(fill='y')

generations = tk.Entry(param_entries_frame)
generations.pack(fill='y',padx=10, pady=10)
generations_label = tk.Label(par_name_frame, text='Generations',pady=10)
generations_label.pack(fill='y')

seed_pos_x = tk.Entry(param_entries_frame)
seed_pos_x.pack(fill='y',padx=10, pady=10)
seed_pos_x_label = tk.Label(par_name_frame, text='Seed position X',pady=10,padx=10)
seed_pos_x_label.pack(fill='y')

seed_pos_y = tk.Entry(param_entries_frame)
seed_pos_y.pack(fill='y',padx=10, pady=10)
seed_pos_y_label = tk.Label(par_name_frame, text='Seed position Y',pady=10)
seed_pos_y_label.pack(fill='y')

colormap = tk.Entry(param_entries_frame)
colormap.pack(fill='y',padx=10, pady=10)
colormap_label = tk.Label(par_name_frame, text='Colormap',pady=10)
colormap_label.pack(fill='y')

interval = tk.Entry(param_entries_frame)
interval.pack(fill='y',padx=10, pady=10)
interval_label = tk.Label(par_name_frame, text='Interval (ms)',pady=10)
interval_label.pack(fill='y')

save_var= tk.IntVar()
save = tk.Checkbutton(par_name_frame, text='Save animation', variable=save_var,pady=10)
save.pack(fill='y',padx=10, pady=5)

info = tk.Label(button_frame, text="* Obligatory fields\n\n Game developped by Daniel FELIN, Nikos KOUGIONIS and Leonir MARCON \nin CentraleSupélec",
                font=('TimesNewRoman', 8, 'italic'), wraplength=225)
info.pack(fill='both',anchor='n', side='top')

run = tk.Button(button_frame,bd=10, text='Run',font=('Impact', 15),command=run, activebackground='green',activeforeground='white'
                ,disabledforeground='black',background='lightgreen',relief='raised', width=25)
run.pack(fill='y', padx=10, pady=10)

univ_size.focus_set() #set the keyboard initially on the first entry box

#============== MAINLOOP FUNCTION ================================================
window.mainloop()
