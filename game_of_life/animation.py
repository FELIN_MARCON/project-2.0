import matplotlib.pyplot as plt
import matplotlib.animation as animation            # the function wouldn't work if the library was not imported inside it...

def animation(universe_history, colormap, interval, save_option):              # the function takes in as arguments the 3D array of the history of a universe
    import matplotlib.animation as animation
    fig = plt.figure()                                  # creation of a figure (space in which the game will be plotted)
    plt.axis('off')                                     # toggling the axis lines off
    frames = []                                         # creation of a list that will contain each frame of our animation
    for i in range(len(universe_history)):              # iteration over the quantity of states that the universe had
        frame = plt.imshow(universe_history[i], cmap = colormap, animated=True)   # attribution of one frame with the function imshow, that associates the matrix with an "image"
        frames.append([frame])                          # appending the frames to a the list of frames

    ani = animation.ArtistAnimation(fig, frames, interval=interval, repeat=False, blit=True) # running the animation, on the figure 'fig'. Interval in ms
    plt.show()
    if save_option: ani.save('animation.html', writer='html')


def return_animation(universe_history, colormap, interval, save_option):
    fig = plt.figure()                                  # creation of a figure (space in which the game will be plotted)
    plt.axis('off')                                     # toggling the axis lines off
    frames = []                                         # creation of a list that will contain each frame of our animation
    for i in range(len(universe_history)):              # iteration over the quantity of states that the universe had
        frame = plt.imshow(universe_history[i], cmap = colormap, animated=True)   # attribution of one frame with the function imshow, that associates the matrix with an "image"
        frames.append([frame])                          # appending the frames to a the list of frames

    ani = animation.ArtistAnimation(fig, frames, interval=interval, repeat=False, blit=True) # running the animation, on the figure 'fig'. Interval in ms
    if save_option: ani.save('animation.html', writer='html')
    return ani
