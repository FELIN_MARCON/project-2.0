from create_seed import *
from generate_universe import *
from add_seed_to_universe import *

def test_add_seed_to_universe():
    seed = create_seed(type_seed = "r_pentomino")                                #create an numpy matrix from the form pentomino
    universe = generate_universe(size=(6,6))                                     #creates a matrix 6x6 filled with zeros
    print(universe)
    universe = add_seed_to_universe(seed, universe,x_start=1, y_start=1)         #inserts the seed in universe
    print("\n", universe)

    test_equality = numpy.array(universe) == numpy.array([[0,0, 0, 0, 0, 0],     #checks if the seed is being correctly insert in the universe
 [0, 0, 1, 1, 0, 0],
 [0, 1, 1, 0, 0, 0],
 [0 ,0, 1, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0]],dtype=numpy.uint8)

    assert test_equality.all()
