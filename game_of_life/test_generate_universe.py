from generate_universe import *
from create_seed import *
from add_seed_to_universe import *
from survival import *
import pytest
import numpy

def test_generate_universe():
    test_array = numpy.array(([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]))       #creating and converting to numpy array an array 4x4 filled just with zeros
    assert numpy.array_equal(generate_universe((4,4)), test_array)              #testing if the result of our function is correct

def test_create_seed():
    seed = create_seed(type_seed = "r_pentomino")
    test_seed = numpy.array([[0, 1, 1],[1, 1, 0],[0, 1, 0]])                    #check if the seed given by the function create_seed is equals to the desired one
    assert numpy.array_equal(seed, test_seed)

def test_add_seed_to_universe():
    seed = create_seed(type_seed = "r_pentomino")                               #create an numpy matrix from the form pentomino
    universe = generate_universe(size=(6,6))                                    #creates a matrix 6x6 filled with zeros
    universe = add_seed_to_universe(seed, universe,x_start=1, y_start=1)        #inserts the seed in universe
    test_equality = numpy.array(universe) == numpy.array([                      #checks if the seed is being correctly insert in the universe
    [0, 0, 0, 0, 0, 0],
    [0, 0, 1, 1, 0, 0],
    [0, 1, 1, 0, 0, 0],
    [0 ,0, 1, 0, 0, 0],
    [0 ,0, 0, 0, 0, 0],
    [0 ,0, 0, 0, 0, 0]],dtype=numpy.uint8)
    assert test_equality.all()

def test_survival():
    test_universe = generate_universe((6,6))                # generates an universe
    test_seed = [[0, 1, 1],                                 # arbitrary seed
                 [1, 1, 0],
                 [0, 1, 0]]

    assert survival(test_universe, (5,5)) == 0               # checks the next value based on our analysis of the seeded universe
    assert survival((add_seed_to_universe(test_seed, test_universe, 1, 1)), (1,1)) == 1
    assert survival((add_seed_to_universe(test_seed, test_universe, 1, 1)), (2,2)) == 0
