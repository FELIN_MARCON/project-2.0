from create_seed import *
import numpy
import pytest

def test_create_seed():
    seed = create_seed(type_seed = "r_pentomino")               #check if the seed given by the function create_seed is equals to the desired one
    test_seed = numpy.array([[0, 1, 1],[1, 1, 0],[0, 1, 0]])
    assert numpy.array_equal(seed, test_seed)

    with pytest.raises(Exception):
        seed_2 = create_seed("inexisting seed")

