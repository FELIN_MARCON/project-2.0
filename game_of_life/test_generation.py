from generation import *
import numpy


def test_generation():
    universe = [
        [0, 1, 1, 1, 1],
        [0, 1, 0, 0, 0],
        [1, 0, 0, 0, 1],
        [1, 1, 0, 1, 1],
        [1, 0, 0, 0, 0]
    ]
    expected_next_generation = [
        [0, 1, 1, 1, 0],
        [1, 1, 0, 0, 1],
        [1, 0, 1, 1, 1],
        [1, 1, 0, 1, 1],
        [1, 1, 0, 0, 0]
    ]

    next_generation = generation(universe)
    print(next_generation)
    assert numpy.array_equal(expected_next_generation, next_generation)

