from game_life_simulate import *
import pytest
import numpy

def test_game_life_simulate():
    universe_start_condition = [
         [0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0],
         [0, 1, 1, 1, 0],
         [0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0],
    ]
    universe_start = numpy.array(universe_start_condition)

    test_array = [[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 1, 1, 1, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]
    resulting_array = game_life_simulate(2, universe_start)
    value_test =  resulting_array[-1] == numpy.array(test_array)
    assert value_test.all()

