def add_seed_to_universe(seed, universe, x_start=0, y_start=0):  #seed is the inicial condition, given by the function create_seed, universe is a matrix filled with zeros, and the other two parameters are the "reference" of the system
    try:
        for i in range(len(seed)):
            y_univ = y_start + i
            for j in range(len(seed[0])):
                x_univ = x_start + j
                universe[y_univ][x_univ] = seed[i][j]
        return universe
    except IndexError:
        print("The seed is bigger than the Universe")
