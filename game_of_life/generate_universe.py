import numpy

def generate_universe(size):
    universe = []                           #grid where the simulation will happen
    for i in range(0,size[0]):              #creating 'size[0]' empty vectors, which will be filled with zeros
        lines = []
        for j in range(0,size[1]):          #fulling each one of the i-th vectors with 'size[1]' zeros
            lines.append (0)
        universe.append(lines)              #appending each of the zero-vectors to our grid
    return numpy.array(universe)            #converting our array to a numpy array
