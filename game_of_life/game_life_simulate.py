from generation import *
import numpy

def game_life_simulate(number_of_rounds, universe):                                     # in this function, the universe must be already added
    universe_history = []                                                                   # array that will keep the history of universes
    new_generation = universe.copy()
    for i in range(number_of_rounds):                                                       # generates a new matrix using the surving rules
        universe_history.append(new_generation)
        new_generation = generation(new_generation)
    universe_history.append(new_generation)
    return universe_history                                                                 # returns the history
