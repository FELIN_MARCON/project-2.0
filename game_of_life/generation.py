from survival import *

def generation(universe):
    # We are creating an empty array the same size with the Universe and then we give it the values of the next state for each cell.
    next_generation = [[None for y in range(len(universe[0]))] for x in range(len(universe))]
    for y in range(len(universe)):
        for x in range(len(universe[0])):
            next_generation[x][y] = survival(universe, (x, y))

    return next_generation
