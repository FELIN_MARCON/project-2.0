import numpy



def create_seed(type_seed):
    seeds = {                           #creating a dictonary of possible seeds
        "boat": [
            [1, 1, 0],
            [1, 0, 1],
            [0, 1, 0],
        ],
        "r_pentomino": [
            [0, 1, 1],
            [1, 1, 0],
            [0, 1, 0],
        ],
        "acorn": [
            [0, 1, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 0],
            [1, 1, 0, 0, 1, 1, 1],
        ],
        "block_switch_engine": [
            [0, 0, 0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 1, 0, 1, 1],
            [0, 0, 0, 0, 1, 0, 1, 0],
            [0, 0, 0, 0, 1, 0, 0, 0],
            [0, 0, 1, 0, 0, 0, 0, 0],
            [1, 0, 1, 0, 0, 0, 0, 0],
        ],
        "infinite": [
            [1, 1, 1, 0, 1],
            [1, 0, 0, 0, 0],
            [0, 0, 0, 1, 1],
            [0, 1, 1, 0, 1],
            [1, 0, 1, 0, 1],
        ],
        "block" : [
             [1,1],
             [1,1],
        ],
         "blinker" : [
             [0,1,0],
             [0,1,0],
             [0,1,0],
        ],
        "toad": [
            [0,1,1,1],
            [1,1,1,0],
        ],
        "beacon" : [
            [1,1,0,0],
            [1,1,0,0],
            [0,0,1,1],
            [0,0,1,1],
        ],
        "beehive": [
            [0,1,1,0],
            [1,0,0,1],
            [0,1,1,0],
        ],
        "diehard": [
            [0,0,0,0,0,0,1,0],
            [1,1,0,0,0,0,0,0],
            [0,1,0,0,0,1,1,1],
        ],
        "glider" : [
            [0, 1, 0],
            [0, 0, 1],
            [1, 1, 1],
        ],
        "lwss" : [
            [0, 0, 1, 1, 0],
            [1, 1, 0, 1, 1],
            [1, 1, 1, 1, 0],
            [0, 1, 1, 0, 0],
        ]
    }

    possible_seeds = []                                 #creating an array of the possible seeds
    for key in seeds:
        possible_seeds.append(key)


    if type_seed.lower() not in seeds.keys():               #verify if the program reconizes the given seed in the dict
        print("The possible seeds are: ", possible_seeds)
        raise Exception("The given argument is not a valid seed.")
    else:
        return numpy.array(seeds[type_seed.lower()])        #convert the asked seed into a numpy array
