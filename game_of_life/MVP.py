import argparse
from generate_universe import *
from create_seed import *
from add_seed_to_universe import *
from game_life_simulate import *
from animation import *

"""
:param  tuple (int, int) universe_size: dimensions of the universe
:param seed: (list of lists, np.ndarray) initial starting array
:param seed_position: (tuple (int, int)) coordinates where the top-left corner of the seed array should be pinned
:param cmap: (str) the matplotlib cmap that should be used
:param n_generations: (int) number of universe iterations, defaults to 30
:param interval: (int )time interval between updates (milliseconds), defaults to 300ms
:param save: (bool) whether the animation should be saved, defaults to False
"""


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("universe_size", type = int, help=" Dimension of the universe. As it will be always a squared universe, it takes just one argument")
    parser.add_argument("seed", type = str, help="Initial Starting Numpy Array")
    parser.add_argument("-SPX","--Seed_Position_X", type = int,default=0, help="X starter coordinate for the seed")
    parser.add_argument("-SPY","--Seed_Position_Y", type = int,default=0, help="Y starter coordinate for the seed")
    parser.add_argument("-G","--Generations", type = int, default=30,help="Number of universe iterations")
    parser.add_argument("-CM","--colormap", type = str, default="gray",help="How will the universe shown")
    parser.add_argument("-S","--save_option", type = bool, default=False,help="Wheter the animation should be saved")
    parser.add_argument("-I","--interval", type = int, default=300, help="Interval between uptades, in miliseconds")

    args = parser.parse_args()                                      # pass all the arguments recieved by the user to args

    universe_size = (args.universe_size,args.universe_size)         # atributes each argument to a parameter
    type_of_seed = args.seed
    seed_position = (args.Seed_Position_X,args.Seed_Position_Y)
    n_generations = args.Generations

    colormap = args.colormap                                        # atributes each given argument to a parameter of the matplot show
    save = args.save_option
    interval = args.interval

    empty_universe = generate_universe(universe_size)
    seed = create_seed(type_of_seed)
    universe = add_seed_to_universe(seed, empty_universe, seed_position[1], seed_position[0])
    history_of_universe = game_life_simulate(n_generations, universe)
    animation(history_of_universe, colormap, interval, save)


def game_generator(universe_size, type_of_seed,
                   seed_position=(0, 0), generations=30, colormap="gray", save_option=False, interval=300):

    empty_universe = generate_universe((universe_size, universe_size))
    seed = create_seed(type_of_seed)
    universe = add_seed_to_universe(seed, empty_universe, seed_position[1], seed_position[0])
    history_of_universe = game_life_simulate(generations, universe)
    animation(history_of_universe, colormap, interval, save_option)

if __name__ == '__main__':
    main()
