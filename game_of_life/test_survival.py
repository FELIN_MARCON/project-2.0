from survival import *
from generate_universe import *
from add_seed_to_universe import *



def test_survival():
    test_universe = generate_universe((6,6))
    test_seed = [[0, 1, 1],
                 [1, 1, 0],
                 [0, 1, 0]]

    assert survival(test_universe, (5,5)) == 0
    assert survival((add_seed_to_universe(test_seed, test_universe, 1, 1)), (1,1)) == 1
    assert survival((add_seed_to_universe(test_seed, test_universe, 1, 1)), (2,1)) == 1
