def survival(universe, cell):
    x_limit = len(universe[0])                              # x coordinate of the universe border
    y_limit = len(universe)                                 # y coordinate of the universe border
    neighbors_alive = 0                                     # counter for live cells in neighborhood
    cell_next_state = 0                                     # the next state is dead by default, it has to match the conditions to remain or become alive

    for x in range(cell[0]-1, cell[0]+2):                   # iterating over the neighbor columns of the cell
        for y in range (cell[1]-1, cell[1]+2):              # iterating over the neighbor lines of the cell
            if x>=0 and y>=0 and x<x_limit and y<y_limit and not (x == cell[0] and y == cell[1]): # condition in order to stop at the borders and to not count the cell itself
                if universe [x][y] == 1:
                    neighbors_alive += 1                    # checking nad countinge the live neighbor cells

    if universe[cell[0]][cell[1]] == 1:                     # conditions for a live cell
        if neighbors_alive >= 2 and neighbors_alive <= 3:
            cell_next_state = 1
    if universe[cell[0]][cell[1]] == 0:                     # conditions for a dead cell
        if neighbors_alive == 3:
            cell_next_state =1
    return cell_next_state
