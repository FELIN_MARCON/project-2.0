import tkinter as tk
from simulate_universe import simulate_universe
from game_of_life.GUI import *
from gui import *

def run():
    start_game_of_life()


def gas():
    display_gui()


def callback(event):
    run()

#============ MAIN WINDOW AND FRAME ========================


window = tk.Tk()
window.geometry('550x300')
window.title("Physics Zoo")
window.minsize(550, 300)

main_frame = tk.Frame(window, bd=15, relief='sunken')
main_frame.pack(expand=1, fill='both')

#============ FRAMES FOR WIDGETS ====================
middle = tk.Frame(main_frame, )
middle.pack(fill='both', anchor='n')

header_frame = tk.Frame(middle)
header_frame.pack(fill='both', expand=1, anchor='n')

param_frame = tk.Frame(middle)
param_frame.bind_all('<Return>', callback)
param_frame.pack(side='top')

par_name_frame = tk.Frame(param_frame)
par_name_frame.pack(fill='both', side='left', anchor='w')

param_entries_frame = tk.Frame(param_frame)
param_entries_frame.pack(fill='both', side='right', anchor='e')

button_frame = tk.Frame(middle)
button_frame.pack(fill='both',anchor='s', side='bottom')

#-------------- header -----------------------------------
header = tk.Label(header_frame, text='Physics Zoo', pady=15, font=('System', 26, 'bold'), relief='sunken', bd=15)
header.pack(fill='both', expand=1)

run_but = tk.Button(button_frame,bd=10, text='Game Of Life',font=('Impact', 15),command=run, activebackground='green', activeforeground='white'
                , disabledforeground='black', background='lightgreen', relief='raised', width=25)
run_but.pack(fill='y', padx=15, pady=20, side=tk.RIGHT)

infos_button = tk.Button(button_frame, bd=10, text='Gas Simulation', font=('Impact', 15),command=gas, activebackground='green', activeforeground='white'
                , disabledforeground='black', background='lightgreen', relief='raised', width=25)
infos_button.pack(fill='y', padx=15, pady=20, side=tk.LEFT)


#============== MAINLOOP FUNCTION ================================================
window.mainloop()

