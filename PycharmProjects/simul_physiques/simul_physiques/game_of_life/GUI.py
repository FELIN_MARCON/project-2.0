import tkinter as tk
from game_of_life.MVP import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

ani = None


def start_game_of_life():

    def run():
        global ani
        universe_size=univ_size.get()
        seed_x = seed_pos_x.get()
        seed_y = seed_pos_y.get()
        num_of_gen = generations.get()
        time = interval.get()
        ani = game_generator(int(universe_size), seed_type.get(), (int(seed_x), int(seed_y)), int(num_of_gen), colormap.get(), int(time))


    #============ MAIN WINDOW ========================
    window= tk.Tk()
    window.geometry('900x600')
    window.title("Game of Life")
    window.resizable(0, 0)
    ani = None
    #============ FRAME WHERE THE PARAMETERS ARE ===========================
    param = tk.Frame(window, bd=15, relief='sunken' )
    param.place(x=0, y=0, height=600, width=300)

    #============= PARAMETERS GADGETS =====================================
    header = tk.Label(param, text='Game of Life', pady=5, font=('Helvetica', 15, 'bold'))
    header.grid(row=0, columnspan=2, stick='e')

    univ_size = tk.Entry(param)
    univ_size.grid(row=1, column=1)
    univ_size_label = tk.Label(param, text='Universe size', padx=10)
    univ_size_label.grid(row=1, column=0)

    seed_type = tk.Entry(param)
    seed_type.grid(row=2, column=1)
    seed_label = tk.Label(param, text='Seed', pady=10)
    seed_label.grid(row=2, column=0)

    generations = tk.Entry(param)
    generations.grid(row=3, column=1)
    generations_label = tk.Label(param, text='Generations')
    generations_label.grid(row=3, column=0)

    seed_pos_x = tk.Entry(param)
    seed_pos_x.grid(row=4, column=1)
    seed_pos_x_label = tk.Label(param, text='Seed position X')
    seed_pos_x_label.grid(row=4, column=0)

    seed_pos_y = tk.Entry(param)
    seed_pos_y.grid(row=5, column=1)
    seed_pos_y_label = tk.Label(param, text='Seed position Y')
    seed_pos_y_label.grid(row=5, column=0)

    colormap = tk.Entry(param)
    colormap.grid(row=6, column=1)
    colormap_label = tk.Label(param, text='Colormap')
    colormap_label.grid(row=6, column=0)

    interval = tk.Entry(param)
    interval.grid(row=7, column=1)
    interval_label = tk.Label(param, text='Interval (ms)')
    interval_label.grid(row=7, column=0)

    run = tk.Button(param, text='Run', font=('Impact', 15), command=run, activebackground='lightgreen', relief='raised')
    run.place(x=80, y=500, width=100, height=50)

    univ_size.focus_set() #set the keyboard initially on the first entry box


    #=============== FRAME WHERE THE GAME WILL BE SHOWED ===========================
    simulation_area = tk.Frame(window, relief='sunken')
    simulation_area.place(x=300, width=600, height=600,)

    graph = tk.Canvas(simulation_area, width=600, height=600, bg='black')
    if ani is not None:
        graph = FigureCanvasTkAgg(ani, master=tk)
    graph.pack()

    #============== MAINLOOP FUNCTION ================================================
    tk.mainloop()
